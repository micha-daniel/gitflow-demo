# gitflow-demo

## Beschreibung
Dieses Projekt ist eine Demo für GitFlow. Es soll aufzeigen, wie die Arbeit mit GitFlow funktioniert

## Für wenn
Das Projekt ist für alle, welche GitFlow lernen wollen. Es dient hauptsächlich für die Node.js Schulung.
